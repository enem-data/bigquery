from pathlib import Path


def main():
    curdir = Path('.').resolve()

    avaliable = [
        fl
        for fl in curdir.iterdir()
        if fl.is_file() and fl.suffix == '.sql' and not fl.name.startswith('_')
    ]
    print("Available views:\n" + '\n'.join(fl.name[:-4] for fl in avaliable))

    target = input("Views to be applied (empty for all): ")
    if not target:
        pass


if __name__ == '__main__':
    main()
