-- CONTROL
-- type: view
-- prefix: answer
-- dependencies: ensureInt
-- END
WITH
  raw_source AS (
  SELECT
    NU_INSCRICAO AS inscricao,
    --     NULL as idade,
    CAST(IDADE AS INT64) AS idade,
    CAST(TP_SEXO AS STRING) AS sexo,
    CAST(ST_CONCLUSAO AS STRING) AS status_conclusao,
    2012 as ano,
    (2012 - `${project_id}.${dataset}.ensureInt`(ANO_CONCLUIU)) AS tempo_conclusao,
    CAST(PK_COD_ENTIDADE AS STRING) AS cod_escola,
    CAST(TP_ESCOLA AS STRING) AS tp_escola,
    CAST(UF_ESC AS STRING) AS uf_escola,
    CAST(COD_MUNICIPIO_ESC AS STRING) AS minicipio_escola,
    CAST(UF_MUNICIPIO_PROVA  AS STRING) AS uf_prova,
    CAST(COD_MUNICIPIO_PROVA AS STRING) AS municipio_prova,
    CAST(TP_LINGUA AS string) AS lingua,
    [ STRUCT("CN" AS area_prova,
      CAST(IN_PRESENCA_CN AS STRING) AS presenca,
      CAST(ID_PROVA_CN AS STRING) AS id_prova,
      CAST(NU_NT_CN AS FLOAT64) AS nota,
      TX_RESPOSTAS_CN AS respostas),
    STRUCT("CH" AS area_prova,
      CAST(IN_PRESENCA_CH AS STRING) AS presenca,
      CAST(ID_PROVA_CH AS STRING) AS id_prova,
      CAST(NU_NT_CH AS FLOAT64) AS nota,
      TX_RESPOSTAS_CH AS respostas),
    STRUCT("LC" AS area_prova,
      CAST(IN_PRESENCA_LC AS STRING) AS presenca,
      CAST(ID_PROVA_LC AS STRING) AS id_prova,
      CAST(NU_NT_LC AS FLOAT64) AS nota,
      TX_RESPOSTAS_LC AS respostas),
    STRUCT("MT" AS area_prova,
      CAST(IN_PRESENCA_MT AS STRING) AS presenca,
      CAST(ID_PROVA_MT AS STRING) AS id_prova,
      CAST(NU_NT_MT AS FLOAT64) AS nota,
      TX_RESPOSTAS_MT AS respostas) ] AS data
  FROM
    `${project_id}.${dataset}.raw_2012`),
  subject_unnested AS (
  SELECT
    raw_source.* EXCEPT (data),
    DATA.* EXCEPT (presenca)
  FROM
    raw_source
  CROSS JOIN
    UNNEST(raw_source.data) AS data
  WHERE
    data.presenca = "1")
SELECT
  subject_unnested.* EXCEPT(respostas),
  resposta,
  (answer_offset + 1) AS item_num_in_area
FROM
  subject_unnested
CROSS JOIN
  UNNEST(SPLIT(subject_unnested.respostas, '')) AS resposta
WITH
OFFSET
  AS answer_offset;
