-- CONTROL
-- type: view
-- prefix: answer
-- dependencies: ensureInt
-- END
WITH
  raw_source AS (
  SELECT
    NU_INSCRICAO AS inscricao,
    --     NULL as idade,
    CAST(NU_IDADE AS INT64) AS idade,
    CAST(TP_SEXO AS STRING) AS sexo,
    CAST(TP_ST_CONCLUSAO AS STRING) AS status_conclusao,
    2009 as ano,
    CAST(NULL AS INT64) AS tempo_conclusao,
    CAST(CO_ESCOLA AS STRING) AS cod_escola,
    CAST(TP_ENSINO AS STRING) AS tp_escola,
    CAST(SG_UF_ESC AS STRING) AS uf_escola,
    CAST(CO_MUNICIPIO_ESC AS STRING) AS minicipio_escola,
    CAST(SG_UF_PROVA AS STRING) AS uf_prova,
    CAST(CO_MUNICIPIO_PROVA AS STRING) AS municipio_prova,
    "N/A" AS lingua,
    [ STRUCT("CN" AS area_prova,
      CAST(TP_PRESENCA_CN AS STRING) AS presenca,
      CAST(CO_PROVA_CN AS STRING) AS id_prova,
      CAST(NU_NOTA_CN AS FLOAT64) AS nota,
      TX_RESPOSTAS_CN AS respostas),
    STRUCT("CH" AS area_prova,
      CAST(TP_PRESENCA_CH AS STRING) AS presenca,
      CAST(CO_PROVA_CH AS STRING) AS id_prova,
      CAST(NU_NOTA_CH AS FLOAT64) AS nota,
      TX_RESPOSTAS_CH AS respostas),
    STRUCT("LC" AS area_prova,
      CAST(TP_PRESENCA_LC AS STRING) AS presenca,
      CAST(CO_PROVA_LC AS STRING) AS id_prova,
      CAST(NU_NOTA_LC AS FLOAT64) AS nota,
      TX_RESPOSTAS_LC AS respostas),
    STRUCT("MT" AS area_prova,
      CAST(TP_PRESENCA_MT AS STRING) AS presenca,
      CAST(CO_PROVA_MT AS STRING) AS id_prova,
      CAST(NU_NOTA_MT AS FLOAT64) AS nota,
      TX_RESPOSTAS_MT AS respostas) ] AS data
  FROM
    `${project_id}.${dataset}.raw_2009`),
  subject_unnested AS (
  SELECT
    raw_source.* EXCEPT (data),
    DATA.* EXCEPT (presenca)
  FROM
    raw_source
  CROSS JOIN
    UNNEST(raw_source.data) AS data
  WHERE
    data.presenca = "1")
SELECT
  subject_unnested.* EXCEPT(respostas),
  resposta,
  (answer_offset + 1) AS item_num_in_area
FROM
  subject_unnested
CROSS JOIN
  UNNEST(SPLIT(subject_unnested.respostas, '')) AS resposta
WITH
OFFSET
  AS answer_offset;
